import React from 'react';

let DeleteButton = function (props) {
    let {handler, item_id} = props;
    return (
        <button onClick={()=>handler(item_id)} type="button">Delete</button>
    )
};

let withHandler = function (Component) {
    return function (props) {
        return (
            <Component {...props} />
        )
    }
};

export default withHandler(DeleteButton)


