import React from 'react'
import ListItem from './ListItem'


export default function (props) {
    let {items, deleteHandler, editHandler} = props;
    return (
        <ul>
            {items.map(item => <ListItem key={item.id} item={item} deleteHandler={deleteHandler} editHandler={editHandler}/>)}
        </ul>
    )
}
