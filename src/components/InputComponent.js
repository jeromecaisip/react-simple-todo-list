import React from 'react';
import {withHandler} from "./utils";

let TaskInput = function (props) {
    let {handler} = props;
    return (
        <form onSubmit={(e) => {
            e.preventDefault();
            handler(e.target[0].value);
            e.target[0].value = '';
        }}>
            <input type="text"/>
            <input type="submit" value="Submit"/>
        </form>
    )
};


class TaskEditInput extends React.Component {
    constructor(props) {
        super(props);
        let {handler, item_id, change_editable} = this.props;
        this.handler = handler
        this.item_id = item_id;
        this.change_editable = change_editable
        this.state = {
            form_value: ''
        }
        this.handleChange = this.handleChange.bind(this)
        this.update = this.update.bind(this)
    }

    handleChange(value) {
        this.setState({form_value: value})
    }

    update(value, id) {
        this.handler(value, id);
    }

    render() {
        return (
            <form onSubmit={function (event) {
                event.preventDefault();
                this.handler(event.target[0].value, this.item_id);
                event.target[0].value = '';
                this.change_editable(event);
            }.bind(this)}
            >
                <input type="text"
                       value={this.state.form_value}
                       onChange={(e) => {
                           this.handleChange(e.target.value);
                       }}
                />
                <button type="button" onClick={()=>this.update(this.state.form_value,this.item_id)}>Edit</button>
            </form>
        )
    }

}

let WithUpdateHandler = withHandler(TaskEditInput);

class Editable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editable: false
        };
        this.changestate = this.changestate.bind(this)
    }

    changestate(e) {
        if (e.target.type === 'text') {
            return
        }
        let editablestate = this.state.editable;
        this.setState({editable: !editablestate});
    }


    render() {
        //Fix Dynamic rendering of Editable; Form not connected.
        let children;
        if (this.state.editable) {
            children = <WithUpdateHandler handler={this.props.handler}
                                          item_id={this.props.item_id}
                                          change_editable={this.changestate}/>
        } else {
            children = this.props.children
        }
        return (
            <div onClick={this.changestate}>
                {children}
            </div>
        )
    }
}


let AddTaskInput = withHandler(TaskInput);

export {
    Editable,
    AddTaskInput,
}
