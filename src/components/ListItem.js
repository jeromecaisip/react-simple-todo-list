import React from 'react'
import ButtonWithHandler from './DeleteButton'
import {Editable} from "./InputComponent";

export default function (props) {
    let {item, deleteHandler, editHandler} = props;
    return (
        <Editable handler={editHandler} item_id={item.id}>
        <li>
            {item.text}
            <ButtonWithHandler handler={deleteHandler} item_id={item.id} />
        </li>
        </Editable>
    )
}
