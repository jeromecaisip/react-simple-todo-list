import React from "react";

let withHandler = function (Component) {
    return function (props) {
        return (
            <Component {...props} />
        )
    }
};


export {
    withHandler,
}
