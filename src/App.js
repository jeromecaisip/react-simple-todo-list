import React, {Component} from 'react';
import './App.css';
import TodoList from './components/ListComponent'
import {AddTaskInput} from "./components/InputComponent";

function getLargestId(items) {
    let maxId = items.reduce(function (maxId, item) {
        return Math.max(maxId, item.id)
    }, 0);
    return maxId
}

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            items: [
                {id: 1, text: 'test'},
                {id: 2, text: 'test1'},
                {id: 3, text: 'test2'},
                {id: 4, text: 'test2'},
            ]
        };
        this.deleteItem = this.deleteItem.bind(this);
        this.addItem = this.addItem.bind(this);
        this.editHandler = this.editHandler.bind(this);
    }

    addItem(text) {
        let largestId = getLargestId([...this.state.items]);
        let new_task = {
            id: largestId + 1,
            text: text,
        };
        let items = [...this.state.items, new_task];
        this.setState({items: items})
    }

    deleteItem(id) {
        let items = [...this.state.items];
        let index = items.findIndex(function (item) {
            return item.id === id;
        });
        items.splice(index, 1);
        this.setState({items: items})
    }

    editHandler(text, id) {
        let items = [...this.state.items];
        let index = items.findIndex(function (item) {
            return item.id === id;
        });
        let edited_task = {
            ...items[index],
            text: text
        };
        items[index] = edited_task;
        this.setState({items: items});
    }

    render() {
        return (
            <div>
                <h1>Todo</h1>
                <AddTaskInput handler={this.addItem}/>
                <TodoList items={this.state.items} deleteHandler={this.deleteItem} editHandler={this.editHandler}/>
            </div>
        );
    }
}

export default App;
